class CreateSupport < ActiveRecord::Migration[5.2]
  def change
    create_table :support do |t|
      t.string :name
      t.string :phone
      t.string :email
      t.string :company
      t.string :subject
      t.string :description
      t.integer :assigned_to 
      t.integer :contacted, :default => 0
      t.string :outcome
      t.timestamps
    end
  end
end
