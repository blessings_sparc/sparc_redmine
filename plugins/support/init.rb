Redmine::Plugin.register :support do
  name 'Support plugin'
  author 'Blessings Mhango'
  description 'This is plugin for support'
  version '0.0.1'
  url ''
  author_url ''

  menu :application_menu, :supports, { :controller => 'support', :action => 'contactsupport' }, :caption => 'Contact Support'

  delete_menu_item :top_menu, :my_page
  delete_menu_item :top_menu, :help
  delete_menu_item :project_menu, :overview
  delete_menu_item :project_menu, :activity
  delete_menu_item :project_menu, :news
end
