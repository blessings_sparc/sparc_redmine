class SupportController < ApplicationController
  def index
    @issues = Support.all
  end

  def contactsupport
    
    unless request.post?
      @support = Support.new
    else
      @support = Support.new(support_params)
      if @support.save
        flash[:notice] = 'You case has been submitted successfully.We will contact you shortly.'
        redirect_to :action => 'contactsupport'
      else
        redirect_to new_support_path, alert: "Error submitting case."
      end  
    end
  end


  private
  def support_params
    params.require(:support).permit(:name, :phone, :email, :company, :subject, :description)
  end

end
